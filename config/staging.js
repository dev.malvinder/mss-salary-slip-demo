const db = require('./db');

module.exports = {
  host: "0.0.0.0",
  port: "3100",
  secretKey: "",
  mailOpts: {
    host: "",
    port: 587,
    secure: false,
    username: "",
    password: ""
  },
  env: "staging",
  db: db.staging
};