module.exports = {
  development: {
    mysql: {
      host: "localhost",
      port: 3306,
      dialect: "mysql",
      username: "root",
      password: "password",
      dbName: "dummyDB",
      minPoolSize: 2,
      maxPoolSize: 5
    },
    mongo: {
      host: "localhost",
      port: 27017,
      username: "",
      password: "",
      dbName: "dummyDB"
    },
    redis: {
      host: "localhost",
      port: 6379,
      timeout: 86400, // in seconds
    }
  },

  staging: {
    mysql: {
      host: "localhost",
      port: 3306,
      dialect: "mysql",
      username: "stageApp",
      password: "password123",
      dbName: "dummyDBStage",
      minPoolSize: 5,
      maxPoolSize: 10
    },
    mongo: {
      host: "localhost",
      port: 27017,
      username: "",
      password: "",
      dbName: "dummyDBStage"
    },
    redis: {
      host: "localhost",
      port: 6379,
      timeout: 86400, // in seconds
    }
  },

  production: {
    mysql: {
      host: "localhost",
      port: 3306,
      dialect: "mysql",
      username: "user",
      password: "P@ssw0rd",
      dbName: "dummy",
      minPoolSize: 10,
      maxPoolSize: 50
    },
    mongo: {
      host: "localhost",
      port: 27017,
      username: "",
      password: "",
      dbName: "dummy"
    },
    redis: {
      host: "localhost",
      port: 6379,
      timeout: 86400, // in seconds
    }
  }
};