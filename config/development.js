const db = require('./db');

module.exports = {
  host: "0.0.0.0",
  port: "3105",
  secretKey: "",
  mailOpts: {
    host: "",
    port: 587,
    secure: false,
    username: "",
    password: ""
  },
  env: "development",
  db: db.development
};