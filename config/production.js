const db = require('./db');

module.exports = {
  host: "0.0.0.0",
  port: "3300",
  secretKey: "",
  mailOpts: {
    host: "",
    port: 587,
    secure: false,
    username: "",
    password: ""
  },
  env: "production",
  db: db.production
};