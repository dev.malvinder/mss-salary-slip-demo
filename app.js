const app = require('./bin/server');
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
const debug = require('debug')('salarySlip:server');
const config = require('config');

const port = config.port;

app.set('port', port);

let server;

const onListening = () => {
  const address = server.address();
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + address.port;
  debug('Listening On: ' + bind);
  console.log('Server running on : ' + port);

}

// Running the server in cluster mode
if (cluster.isMaster) {
  let i = 0;
  for (i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
}
else {
  server = http.createServer(app);

  server.listen(port);
  // server.on('error', onError);
  server.on('listening', onListening);

  (async() => {
    server.on('listen', onListening);
    console.log('Server Ok');
  })();
}

