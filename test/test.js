const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

const salaryServices = require('../src/services/MonthlyCalculation');

describe("Salary Slip", () => {
  describe("#calculateSalary", () => {
    it("should calculate monthly salary, incomeTax, superAmt", async () => {

      const expectedResult = {
        firstName: "Andrew",
        lastName: "Smith",
        paymentStartDate: "1 March 2018 - 31 March 2018",
        grossIncome: 5004,
        incomeTax: 922,
        netIncome: 4082,
        superAmt: 450
      };

      const payload = {
        firstName: "Andrew",
        lastName: "Smith",
        annualSalary: 60050,
        superRate: 9,
        paymentStartDate: "1 March 2018 - 31 March 2018"
      };

      const salarySlip = await salaryServices.calculateSalarySlip(payload);

      expect(salarySlip).to.eql(expectedResult);
    });
  });
});
