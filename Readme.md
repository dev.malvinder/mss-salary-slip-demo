# Express App to Calculate Salary

_deployed tested on Node v9_

### Modules Used

- Express
- Body-Parser
- Config
- Joi
- Express Joi Validator
- Mocha
- Chai


### To run the application

#### Developer Mode
**npm run dev** `Port 3105`

#### Staging Mode
**npm run stage** `Port 3100`

#### Production Mode
**npm run production** `Port 3300`

#### To check the Test
**npm run test**

### Route

**POST** `/salary-slip`

##### Payload
```
{
	"firstName": "Andrew",
	"lastName": "Smith",
	"annualSalary": 60050,
	"superRate": 9,
	"paymentStartDate": "1 March - 31 March"
}

```