const { calculateSalary } = require('../../helpers/taxCalculations');


const calculateSalarySlip = async payload => {
  const {
    firstName,
    lastName,
    annualSalary,
    superRate,
    paymentStartDate
  } = payload;

  const salarySlip = await calculateSalary(annualSalary, superRate);

  salarySlip.firstName = firstName;
  salarySlip.lastName = lastName;
  salarySlip.paymentStartDate = paymentStartDate;

  return salarySlip;

};


module.exports = {
  calculateSalarySlip
};
