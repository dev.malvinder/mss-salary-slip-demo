const Express = require('express');
const app = Express();
const expJoi = require('express-joi-validator');

const validator = require('../../validators/MonthlyCalculation');
const services = require('../../services/MonthlyCalculation');

const router = Express.Router();

app.use(router);


router
  .post('/', expJoi(validator.salaryCalculation),
  async (req, res, next) => {
    const payload = req.body;

    const responseData = await services.calculateSalarySlip(payload);

    res
      .json({
        type: 'SUCCESS',
        data: responseData
      }).status(201);

});


module.exports = app;
