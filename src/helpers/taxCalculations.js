const {
  taxPrices,
  taxSalary,
  taxRates
} = require('./constants');

const calculateGrossIncome = (annualSalary) => {
  return Math.round(annualSalary / 12);
};

const calculateNetIncome = (grossIncome, incomeTax) => {
  return grossIncome - incomeTax;
};

const calculateSuper = (grossIncome, superRate) => {
  return Math.round(grossIncome * (superRate / 100));
};

const calculateTaxableIncome = (annualSalary, taxedSalary) => {
  console.log('Annual: ', annualSalary);
  console.log('Less Taxed: ', taxedSalary);
  return annualSalary - taxedSalary;
};

const calculateIncomeTax = (taxPrice, taxableIncome, taxRate) => {
  const tax = taxPrice + ( taxableIncome * ( taxRate / 100 ));
  return tax;
};

const calculateTax = annualSalary => {
  let incomeTax = 0;
  let taxableIncome = 0;

  if (annualSalary <= taxSalary.low) {
    incomeTax = taxPrices.low;
    return incomeTax;
  }

  if (annualSalary > taxSalary.low && annualSalary <= taxSalary.basic) {
    taxableIncome = calculateTaxableIncome(annualSalary, taxSalary.low);
    incomeTax = Math.round(calculateIncomeTax(taxPrices.basic, taxableIncome, taxRates.basic));
    incomeTax = Math.round(incomeTax / 12);
    return incomeTax;
  }

  if (annualSalary > taxSalary.basic && annualSalary <= taxSalary.mid) {
    taxableIncome = calculateTaxableIncome(annualSalary, taxSalary.basic);
    incomeTax = Math.round(calculateIncomeTax(taxPrices.mid, taxableIncome, taxRates.mid));
    incomeTax = Math.round(incomeTax / 12);
    return incomeTax;
  }

  if (annualSalary > taxSalary.mid && annualSalary <= taxSalary.moderate) {
    taxableIncome = calculateTaxableIncome(annualSalary, taxSalary.mid);
    incomeTax = Math.round(calculateIncomeTax(taxPrices.moderate, taxableIncome, taxRates.moderate));
    incomeTax = Math.round(incomeTax / 12);
    return incomeTax;
  }

  if (annualSalary > taxSalary.moderate) {
    taxableIncome = calculateTaxableIncome(annualSalary, taxSalary.high);
    incomeTax = Math.round(calculateIncomeTax(taxPrices.high, taxableIncome, taxRates.high));
    incomeTax = Math.round(incomeTax / 12);
    return incomeTax;
  }

}

const calculateSalary = (salary, superRate) => {
  const grossIncome = calculateGrossIncome(salary);
  const incomeTax = calculateTax(salary);
  const netIncome = calculateNetIncome(grossIncome, incomeTax);
  const superAmt = calculateSuper(grossIncome, superRate);

  return {
    grossIncome,
    incomeTax,
    netIncome,
    superAmt
  };
};


module.exports = {
  calculateSalary
};
