exports.taxRates = {
  low: 0,
  basic: 19,
  mid: 32.5,
  moderate: 37,
  high: 45
};

exports.taxPrices = {
  low: 0,
  basic: 0,
  mid: 3572,
  moderate: 19822,
  high: 54232
};

exports.taxSalary = {
  low: 18200,
  basic: 37000,
  mid: 87000,
  moderate: 180000
}
