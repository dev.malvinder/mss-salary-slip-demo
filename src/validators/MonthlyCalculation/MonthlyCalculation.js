const Joi = require('joi');

const salaryCalculation = {
  body: {
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    annualSalary: Joi.number().integer().min(0).required(),
    superRate: Joi.number().min(0).max(12).required(),
    paymentStartDate: Joi.string().required()
  }
};


module.exports = {
  salaryCalculation
};
