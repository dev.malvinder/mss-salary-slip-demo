const Express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const cors = require('cors');

const config = require('config');


const app = Express();

const { env } = config;

if (env === 'development') {
  app.use(logger('dev'));
}
else {
  app.use(logger('combined'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());
app.use(helmet());

/**
 * body-parser breaks when posting invalid JSON - this middleware catches the error
 */
app.use((error, req, res, next) => {
  if(error instanceof SyntaxError && req.method === 'POST') {
  return res.status(400).json({
    error: 'Error parsing JSON'
  })
}
  next();
});


const salarySlip = require('../src/controllers/MonthlyCalculation');

app.use('/salary-slip', salarySlip);


module.exports = app;
